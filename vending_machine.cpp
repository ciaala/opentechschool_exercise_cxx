//
// Created by crypt on 18/09/18.
//

#include "iostream"
#include "string"
#include "vector"
using namespace std;

struct product {
  string name;
  double price;
};
std::vector<product> products = {
    {"coke", 1.5},
    {"cigarettes", 4.5},
    {"zweiffel", 2.5},
    {"zweiffel paprica", 2.5},
    {"<any insurance>", 99.9},
    {"sbb daily ticket", 8.8}
};
std::vector<double> coins = {5, 2, 1, 0.5, 0.2, 0.1, 0.05};
int select_product();

double insert_money(double price) {
  double amount = 0;
  cout << endl << "---- COIN LIST ----" << endl;
  for( auto i = 0 ; i < coins.size(); i++ ) {
    auto &coin  = coins[i];
    cout << "[" << i+1 << "] " << coin << "$]" << endl;
  }
  cout << "----------------------" << endl;

  while( amount < price) {
    cout << "amount paid " << amount << "$ (" << price << ")" << endl;
    cout << "insert coin [1-" << coins.size() << "] >";
    int coinIndex = 0;
    cin >> coinIndex;

    if ( coinIndex >1 && coinIndex <= coins.size() ) {
      amount += coins[--coinIndex];
    }
  }
  return amount;
}

int select_product() {
  cout << "---- PRODUCT LIST ----" << endl;
  for( auto i = 0 ; i < products.size(); i++ ) {
    auto &product  = products[i];
    cout << "[" << i+1 << "] " << product.name << " [" << product.price << "$]" << endl;
  }
  cout << "----------------------" << endl;

  int choosen = 0;
  while( choosen == 0) {
    cout << "Select one product [1-" << products.size() << "] >";
    cin >> choosen;
    if ( choosen > 1 && choosen <= products.size()) {
      choosen -=1;
    }

  }
  return choosen;
}

void give_change(double d);
int main(int argc, char** argv) {
  auto productIndex = select_product();
  auto &product = products[productIndex];
  cout << "You have selected '" << product.name << "', please insert " << product.price << "$";
  auto payment = insert_money(product.price);
  give_change(payment-product.price);
  cout << "You can now get your product " << product.name;
}
void give_change(double remain) {
  while (remain > 0) {
    for ( auto &coin : coins) {
      if (remain > coin ) {
        cout << "get coin " << coin;
        remain -= coin;
      }
    }
  }
}
