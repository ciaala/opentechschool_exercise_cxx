//
// Created by crypt on 18/09/18.
//

#ifndef CTCI_FIBONACCI_HPP
#define CTCI_FIBONACCI_HPP
#include <iostream>
#include <iomanip>
#include <unordered_map>

using namespace std;


long _fibonacci(long n, long depth) {

    cout << std::setw((int)(depth)*2)
        << std::setfill(' ') << "f(" << n << ")" << endl;
  if (n > 1) {
    return _fibonacci(n-1, depth-1) + _fibonacci(n-2, depth-1);
  } else {
    return 1;
  }
}

long fibonacci(long n) {
  return _fibonacci(n,n);
}

unordered_map<long,long> global_mem;


long _fibonacci_memoization(long n, long depth, unordered_map<long,long> &mem) {

  cout << std::setw((int) (depth) * 2)
       << std::setfill(' ') << "f(" << n << ")" << endl;
  if (mem.count(n) > 0 ) {
    return mem[n];
  }
  if (n > 1) {
    auto result = _fibonacci_memoization(n-1, depth-1, mem) + _fibonacci_memoization(n-2, depth-1, mem);
    mem[n] = result;
    return result;
  } else {
    mem[n] = 1;
    return 1;
  }
}


long fibonacci_memoization(long n) {
  unordered_map<long,long> mem;
  return _fibonacci_memoization(n, n , mem);
}

long fibonacci_memoization_global(long n) {
  return _fibonacci_memoization(n, n, global_mem);
}
#endif //CTCI_FIBONACCI_HPP
