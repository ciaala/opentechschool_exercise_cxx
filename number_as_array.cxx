#include <iostream>
#include <vector>

#define V10

class ArrayNumber {

 public:
  std::vector<uint32_t> number;
  ArrayNumber(std::vector<uint32_t> number);
  const ArrayNumber operator+(const ArrayNumber &other);
  friend std::ostream &operator<<(const ArrayNumber &number, std::ostream &ostream);

};

const ArrayNumber ArrayNumber::operator+(const ArrayNumber &other) {
  std::vector<uint32_t> result;
  auto indexThis = this->number.size();
  auto indexOther = other.number.size();
  uint32_t carry = 0;
  while (indexThis > 0 || indexOther > 0) {
    auto digitThis = indexThis > 0 ? this->number[--indexThis] : 0;
    auto digitOther = indexOther > 0 ? other.number[--indexOther] : 0;
    uint32_t digit = digitThis + digitOther + carry;
    if (digit > 9) {
      carry = 1;
      digit -= 10;
    } else {
      carry = 0;
    }
    result.insert(result.begin(), digit);
  }
  if (carry > 0) {
    result.insert(result.begin(), 1);
  }
  return ArrayNumber(result);
}

ArrayNumber::ArrayNumber(std::vector<uint32_t> number) : number(std::move(number)) {

}
std::ostream &operator<<(std::ostream &ostream, const ArrayNumber &number) {
  for (uint32_t digit : number.number) {
    ostream << digit;
  }
  return ostream;
}

int main() {
  {
    ArrayNumber N1 = ArrayNumber({1, 2, 3});
    ArrayNumber N2 = ArrayNumber({5, 7, 8});
    ArrayNumber N = N1 + N2;
    std::cout << N1 << " + " << N2 << " = " << N << std::endl;
  }
  {
    ArrayNumber N1 = ArrayNumber({9, 9, 9});
    ArrayNumber N2 = ArrayNumber({1});
    ArrayNumber N = N1 + N2;
    std::cout << N1 << " + " << N2 << " = " << N << std::endl;
  }
}
