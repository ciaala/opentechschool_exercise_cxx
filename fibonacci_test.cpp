//
// Created by crypt on 18/09/18.
//


#include <cassert>
#include "fibonacci.hpp"

typedef long fibonacci_function(long);

long input[11] = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
long output[11] = {1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89};

int test_generic_fibonacci(string name, fibonacci_function fn) {

  for (int i = 0; i < 11; i++) {
    auto result = fn(input[i]);
    if (result != output[i]) {
      cerr << name << " error expected " << result << " got f(" << i << ")"  << output[i] << endl;
      return 1;
    }
  }
  return 0;
}

int main(int argc, char **argv) {
  //assert(test_generic_fibonacci("fibonacci", fibonacci) == 0 );
  //assert(test_generic_fibonacci("fibonacci_memoization", fibonacci_memoization) == 0 );
  //assert(test_generic_fibonacci("fibonacci_memoization_global", fibonacci_memoization_global) == 0 );
}
